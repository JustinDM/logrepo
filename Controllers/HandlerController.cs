using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;
using Economical.EcoObjects.General.Models;
using Economical.LogRepo.Data;
using Microsoft.AspNetCore.Mvc;

namespace Economical.LogRepo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HandlerController : ControllerBase
    {
        private readonly IHandlerRepository _repo;
        public HandlerController(IHandlerRepository repo)
        {
            _repo = repo;
        }


        [HttpPost]
        public async Task<IActionResult> UploadHandler([FromBody] Handler handler)
        {
            _repo.Add(handler);

            if (await _repo.SaveAll())
            {
                return CreatedAtRoute("GetHandler", new {handlerId = handler.Id}, handler);
            }

            throw new Exception("Failed to upload handler");
        }

        [HttpGet("{handlerId}", Name = "GetHandler")]
        public async Task<IActionResult> GetHandler(Guid handlerId)
        {
            var handler = await _repo.GetHandler(handlerId);

            return Ok(handler);
        }

        [HttpGet]
        public async Task<IActionResult> GetHandlers([FromBody] RepoQueryParams queryParams)
        {
            var handlers = await _repo.GetHandlers(queryParams);

            return Ok(handlers);
        }

        [HttpDelete("{handlerId}")]
        public async Task<IActionResult> DeleteHandler(Guid handlerId)
        {
            var handler = await _repo.GetHandler(handlerId);
            
            _repo.Delete(handler);

            if (await _repo.SaveAll())
            {
                return NoContent();
            }

            throw new Exception("Failed to delete handler");
        }

    }
}