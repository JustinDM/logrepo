using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Economical.LogRepo.Data;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.LogRepo;

namespace Economical.LogRepo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LogController : ControllerBase
    {
        private readonly IRepository _repo;
        public LogController(IRepository repo)
        {
            _repo = repo;
        }

        [HttpPost("handler")]
        public async Task<IActionResult> UploadHandler([FromBody] HandlerParent handler)
        {
            _repo.Add(handler);

            if (await _repo.SaveAll())
            {
                return CreatedAtRoute("GetHandlerParent", new {handlerId = handler.Id}, handler);
            }

            throw new Exception("Failed to upload handler");
        }

        // [HttpGet("handler/{handlerId}", Name = "CcGetHandler")]
        [HttpGet("handler/{handlerId}", Name = "GetHandlerParent")]
        public async Task<IActionResult> GetHandlerParent(Guid handlerId)
        {
            var handler = await _repo.GetHandlerParent(handlerId);

            return Ok(handler);
        }

        [HttpGet("handler")]
        public async Task<IActionResult> GetHandlerParents()
        {
            var handlers = await _repo.GetHandlerParents();

            return Ok(handlers);
        }
    }
}