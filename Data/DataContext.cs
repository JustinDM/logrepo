using Economical.EcoObjects.General.Models;
using Microsoft.EntityFrameworkCore;
using Economical.EcoObjects.LogRepo;
using Economical.EcoObjects.EnterpriseRentalPayments;
using Economical.EcoObjects.CDS;
using Newtonsoft.Json;
using System.Collections.Generic;
using Economical.EcoObjects.CDS.Task;
using Economical.EcoObjects.General;

namespace Economical.LogRepo.Data
{
    public class DataContext : DbContext 
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<HandlerParent> HandlerParents { get; set; }
        public DbSet<ConfigParent> ConfigParents { get; set; }
        public DbSet<UnitParent> UnitParents { get; set; }
        public DbSet<UnitInfoParent> UnitInfoParents { get; set; }
        public DbSet<TraceParent> TraceParents { get; set; }

        public DbSet<Handler> Handlers { get; set; }
        public DbSet<Config> Configs { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<UnitInfo> UnitInfos { get; set; }
        public DbSet<Trace> Traces { get; set; }

        // ERP
        public DbSet<ErpUnitInfo> ErpUnitInfos { get; set; }
        public DbSet<ErpConfig> ErpConfigs { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Handler>()
                .HasOne(h => h.Config)
                .WithOne(c => c.Handler)
                .HasForeignKey<Config>(c => c.HandlerId);
        
            builder.Entity<Unit>()
                .HasOne(u => u.UnitInfo)
                .WithOne(ui => ui.Unit)
                .HasForeignKey<UnitInfo>(ui => ui.UnitId);

            builder.Entity<ErpUnitInfo>()
                .Property(e => e.ReserveInfo)
                .HasConversion(
                    ri => ri.ToString(),
                    ri => new ReserveInfo()
                );

            builder.Entity<ErpUnitInfo>()
                .Property(e => e.PaymentReserve)
                .HasConversion(
                    cr => cr.ToString(),
                    cr => new ClaimReserve(cr)
                );
            
            builder.Entity<ErpUnitInfo>()
                .Property(e => e.Reserves)
                .HasConversion(
                    cr => JsonConvert.SerializeObject(cr, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects}),
                    cr => JsonConvert.DeserializeObject<List<ClaimReserve>>(cr, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects})
                );

            builder.Entity<ErpUnitInfo>()
                .Property(e => e.RentalTask)
                .HasConversion(
                    rt => JsonConvert.SerializeObject(rt, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects}),
                    rt => JsonConvert.DeserializeObject<CdsTask>(rt, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects})
                );

            builder.Entity<ErpUnitInfo>()
                .Property(e => e.Tasks)
                .HasConversion(
                    cr => JsonConvert.SerializeObject(cr, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects}),
                    cr => JsonConvert.DeserializeObject<List<CdsTask>>(cr, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects})
                );

            builder.Entity<ErpConfig>()
                .Property(ec => ec.Timeout)
                .HasConversion(
                    t => t.ToString(),
                    t => new Timeout(t)
                );

            builder.Entity<ErpUnitInfo>()
                .Property(eui => eui.DuplicatePayment)
                .HasConversion(
                    dp => JsonConvert.SerializeObject(dp, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects}),
                    dp => JsonConvert.DeserializeObject<Transaction>(dp, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects})
                );

            builder.Entity<ErpConfig>()
                .Property(ec => ec.ProcessingReportRecipients)
                .HasConversion(
                    ec => JsonConvert.SerializeObject(ec, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects}),
                    ec => JsonConvert.DeserializeObject<string[]>(ec, new JsonSerializerSettings() {TypeNameHandling = TypeNameHandling.Objects})
                );

            builder.Entity<Handler>()
                .HasMany(h => h.Traces)
                .WithOne(t => t.Handler)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Unit>()
                .HasMany(u => u.Traces)
                .WithOne(t => t.Unit)
                .OnDelete(DeleteBehavior.Cascade);
                

            

            
        }

    }
}