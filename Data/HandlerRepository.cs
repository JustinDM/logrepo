using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;
using Economical.EcoObjects.General.Models;
using Economical.LogRepo.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Economical.LogRepo.Data
{
    public class HandlerRepository : IHandlerRepository
    {
        private readonly DataContext _context;
        public HandlerRepository(DataContext context)
        {
            _context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<Handler> GetHandler(Guid handlerId)
        {
            return await _context.Handlers
                .Include(h => h.Config)
                .Include(h => h.Traces)
                .Include(h => h.Units)
                .ThenInclude(u => u.UnitInfo)
                .FirstOrDefaultAsync(h => h.Id == handlerId);
                
        }

        public async Task<ICollection<Handler>> GetHandlers(RepoQueryParams queryParams)
        {
            return await _context.GetHandlers(queryParams);
        }


        
    }
}