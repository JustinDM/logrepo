using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;
using Economical.EcoObjects.General.Models;

namespace Economical.LogRepo.Data
{
    public interface IHandlerRepository
    {
        void Add<T> (T entity) where T : class;
        void Delete<T> (T entity) where T : class;
        Task<bool> SaveAll();

        public Task<ICollection<Handler>> GetHandlers(RepoQueryParams queryParams);
        public Task<Handler> GetHandler(Guid handlerId);
    }
}