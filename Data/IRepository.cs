using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.LogRepo;

namespace Economical.LogRepo.Data
{
    public interface IRepository
    {
        void Add<T> (T entity) where T : class;
        void Delete<T> (T entity) where T : class;
        Task<bool> SaveAll();

        public Task<ICollection<HandlerParent>> GetHandlerParents();
        public Task<HandlerParent> GetHandlerParent(Guid handlerId);
    }
}