using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.LogRepo;
using Microsoft.EntityFrameworkCore;

namespace Economical.LogRepo.Data
{
    public class Repository : IRepository
    {
        private readonly DataContext _context;
        public Repository(DataContext context)
        {
            _context = context;
        }
        public void Add<T> (T entity) where T : class
        {
            _context.Add(entity);
        }
        public void Delete<T> (T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<HandlerParent> GetHandlerParent(Guid handlerId)
        {
            return await _context.HandlerParents
                .Include(h => h.ConfigParent)
                .Include(h => h.TraceParents)
                .Include(h => h.UnitParents)
                .ThenInclude(u => u.UnitInfoParent)
                .Include(h => h.UnitParents)
                .FirstOrDefaultAsync(x => x.Id == handlerId);
        }

        public async Task<ICollection<HandlerParent>> GetHandlerParents()
        {
            return await _context.HandlerParents
                .Include(h => h.ConfigParent)
                .Include(h => h.TraceParents)
                .Include(h => h.UnitParents)
                .ThenInclude(u => u.UnitInfoParent)
                .Include(h => h.UnitParents)
                .ToListAsync();
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        
    }
}