// using AutoMapper;
// using Economical.EcoExtensions;
// using Economical.EcoObjects.ContractorConnection;
// using Economical.EcoObjects.ContractorConnection.Enums;
// using Economical.EcoObjects.General.DTOs;
// using Economical.EcoObjects.General.DTOs.ContractorConnection;
// using Economical.EcoObjects.General.Enums;
// using Economical.EcoObjects.General.Models.Trace;
// using General.DTOs.ContractorConnection;

// namespace Economical.LogRepo.Helpers
// {
//     public class AutoMapperProfiles : Profile
//     {
//         public AutoMapperProfiles()
//         {
//             CreateMap<CcInboundTransactionInfoDto, CcTransactionInfo>()
//                 .ForMember(dest => dest.PayeeType, opt => 
//                 {
//                     opt.MapFrom(src => src.PayeeTypeString.ParseEnumFromDescription<CcPayeeType>());
//                 })
//                 .ForMember(dest => dest.Branch, opt => 
//                 {
//                     opt.MapFrom(src => src.BranchString.ParseEnumFromDescription<CcBranch>());
//                 })
//                 .ForMember(dest => dest.ExpenseType, opt =>
//                 {
//                     opt.MapFrom(src => src.ExpenseTypeString.ParseEnumFromDescription<CcExpenseType>());
//                 })
//                 .ForMember(dest => dest.FeeType, opt =>
//                 {
//                     opt.MapFrom(src => src.FeeTypeString.ParseEnumFromDescription<CcFeeType>());
//                 })
//                 .ForMember(dest => dest.SourceSystem, opt =>
//                 {
//                     opt.MapFrom(src => src.SourceSystemString.ParseEnumFromDescription<EconomicalSystem>());
//                 });

//             CreateMap<InboundTraceDto, Trace>()
//                 .ForMember(dest => dest.Polarity, opt => 
//                 {
//                     opt.MapFrom(src => src.PolarityString.ParseEnumFromDescription<LogPolarity>());
//                 })
//                 .ForMember(dest => dest.Type, opt =>
//                 {
//                     opt.MapFrom(src => src.TypeString.ParseEnumFromDescription<LogType>());
//                 })
//                 .ForMember(dest => dest.System, opt => 
//                 {
//                     opt.MapFrom(src => src.SystemString.ParseEnumFromDescription<EconomicalSystem>());
//                 });

//             CreateMap<CcInboundConfigDto, CcConfig>();
//         }
//     }
// }