using System;
using System.Linq;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Address.Enums;
using Economical.EcoObjects.CDS;
using System.Collections.Generic;
using System.Reflection;
using Economical.EcoObjects.General.Extensions;

namespace Economical.LogRepo.Helpers
{
    public static class Converters
    {
        public static List<ClaimReserve> ParseReservesFromDb(string reserveText)
        {
            var claimReserves = new List<ClaimReserve>();
            
            foreach (var i in reserveText.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
            {
                var reserve = new ClaimReserve(i);
                claimReserves.Add(reserve);
            }

            return claimReserves;
        }

        public static Exception ParseException(this string entryString)
        {
            var entryItems = entryString.Split("|", StringSplitOptions.RemoveEmptyEntries);

            if (entryItems.Length == 0)
            {
                return null;
            }

            Type exceptionType;

            if (entryItems[0].Contains("EcoObjects"))
            {
                var assembly = Assembly.Load("EcoObjects");
                exceptionType = assembly.GetType(entryItems[0]);
            }
            else
            {
                exceptionType = Type.GetType(entryItems[0]);
            }


            var returnException = Activator.CreateInstance(exceptionType) as Exception;

            return returnException;
        }

        public static string ExceptionToString(this Exception ex)
        {
            return $"{ex.GetType().ToString()}|{ex.Message}";
        }

        public static EcoAddress ParseAddress(this string entryString)
        {
            var addressItems = entryString.Split("|", StringSplitOptions.RemoveEmptyEntries);
            
            var itemsLength = addressItems.Length;

            var address = new EcoAddress();

            address.PostalCode = new PostalCode(addressItems[itemsLength - 1]);
            address.Province = addressItems[itemsLength - 2].ParseEnumFromDescription<Province>();
            address.City = addressItems[itemsLength - 3];

            var addressLines = addressItems.Take(itemsLength - 3);

            foreach (var i in addressLines)
            {
                address.AddressLines.Add(i);
            }


            return address;
        }

        public static string AddressToString(this EcoAddress address)
        {
            var addressLinesString = String.Join("|", address.AddressLines);

            return $"{addressLinesString ?? ""}|{address.City}|{address.Province}|{address.PostalCode}";
        }
    }
}