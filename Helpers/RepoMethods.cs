using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;
using Economical.EcoObjects.General.Models;
using Economical.LogRepo.Data;
using Microsoft.EntityFrameworkCore;

namespace Economical.LogRepo.Helpers
{
    public static class RepoMethods
    {
        public static async Task<ICollection<Handler>> GetHandlers(this DataContext context, RepoQueryParams queryParams)
        {
            var processHandlers = context.Handlers
                .Include(h => h.Config)
                .Include(h => h.Traces)
                .Include(h => h.Units)
                .ThenInclude(u => u.UnitInfo)
                .Where(x => queryParams.Processes.Contains(x.Process)).AsQueryable();

            if (queryParams.StartDate.Year > 1)
            {
                processHandlers = processHandlers.Where(x => x.StartDate >= queryParams.StartDate).AsQueryable();
            }

            if (queryParams.EndDate.Year > 1)
            {
                processHandlers = processHandlers.Where(x => x.EndDate <= queryParams.EndDate).AsQueryable();
            }

            var handlers = await processHandlers.ToListAsync();

            return handlers;
        }
        // public async static Task<ICollection<Trace>> GetHandlerTraces(this DataContext context, Guid handlerId, RpaProcess process)
        // {
        //     var handler = await context.HandlerLogs
        //         .Include(h => h.Traces)
        //         .FirstOrDefaultAsync(
        //             x => x.Id == handlerId
        //             && x.Process == process
        //         );

        //         return handler.Traces;
        // }

        // public async static Task<ICollection<Unit>> GetHandlerTransactions(this DataContext context, Guid handlerId, RpaProcess process)
        // {
        //     var handler = await context.HandlerLogs
        //         .Include(h => h.Units)
        //         .FirstOrDefaultAsync(
        //         x => x.Id == handlerId
        //         && x.Process == RpaProcess.IccFlatFees
        //     );

        //     return handler.Units;
        // }

        // public async static Task<Trace> GetTrace(this DataContext context, Guid traceId, RpaProcess process)
        // {
        //     return await context.Traces.Where(
        //         x => x.RelatedToProcess(process)
        //         && x.Id == traceId).FirstAsync();
        // }

        // public async static Task<ICollection<Trace>> GetTraces(this DataContext context, RpaProcess process)
        // {
        //     return await context.Traces.Where(x => x.RelatedToProcess(process)).ToListAsync();
        // }

        // public async static Task<Unit> GetTransaction(this DataContext context, Guid transactionId, RpaProcess process)
        // {
        //     return await context.TransactionLogs
        //         .Include(t => t.UnitInfo)
        //         .FirstOrDefaultAsync(
        //             t => t.Id == transactionId
        //             && t.Process == process
        //         );
        // }

        // public async static Task<ICollection<Unit>> GetTransactions(this DataContext context, RpaProcess process)
        // {
        //     return await context.TransactionLogs
        //         .Include(t => t.UnitInfo)
        //         .Where(t => t.Process == process).ToListAsync();
        // }

        // public async static Task<ICollection<Trace>> GetTransactionTraces(this DataContext context, Guid transactionId, RpaProcess process)
        // {
        //     var transaction = await context.TransactionLogs
        //         .Include(x => x.Traces)
        //         .FirstOrDefaultAsync(
        //             x => x.Id == transactionId
        //             && x.Process == process);

        //     return transaction.Traces;
        // }

    }
}