using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Models;

namespace Economical.LogRepo.Helpers
{
    public static class TraceExtensions
    {
        public static bool RelatedToProcess(this Trace trace, RpaProcess process)
        {
            if (trace.Handler != null)
            {
                return trace.Handler.Process == process;
            }

            if (trace.Unit != null)
            {
                return trace.Unit.Process == process;
            }

            return false;
        }
    }
}