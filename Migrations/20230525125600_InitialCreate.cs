﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LogRepo.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HandlerParents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Data = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HandlerParents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Handlers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Process = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ConfigId = table.Column<Guid>(nullable: true),
                    BotId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Handlers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConfigParents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Data = table.Column<string>(nullable: true),
                    HandlerParentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfigParents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConfigParents_HandlerParents_HandlerParentId",
                        column: x => x.HandlerParentId,
                        principalTable: "HandlerParents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UnitParents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Data = table.Column<string>(nullable: true),
                    HandlerParentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitParents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UnitParents_HandlerParents_HandlerParentId",
                        column: x => x.HandlerParentId,
                        principalTable: "HandlerParents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Configs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    HandlerId = table.Column<Guid>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    ResourceFolderPath = table.Column<string>(nullable: true),
                    TempFolderPath = table.Column<string>(nullable: true),
                    CdsCredentialAssetName = table.Column<string>(nullable: true),
                    ReportName = table.Column<string>(nullable: true),
                    CdsQueueName = table.Column<string>(nullable: true),
                    FisQueueName = table.Column<string>(nullable: true),
                    EmailCredentialsAssetName = table.Column<string>(nullable: true),
                    EmailConfigAssetName = table.Column<string>(nullable: true),
                    EmailBodyFileName = table.Column<string>(nullable: true),
                    NotificationSubject = table.Column<string>(nullable: true),
                    ReportEmailSubject = table.Column<string>(nullable: true),
                    RepoCallerEndpointAssetName = table.Column<string>(nullable: true),
                    GcpKeyPathAssetName = table.Column<string>(nullable: true),
                    VendorCode = table.Column<string>(nullable: true),
                    VendorName = table.Column<string>(nullable: true),
                    ReserveTablePathAssetName = table.Column<string>(nullable: true),
                    ReserveTableFileName = table.Column<string>(nullable: true),
                    ReferenceText = table.Column<string>(nullable: true),
                    Timeout = table.Column<string>(nullable: true),
                    NoteFileName = table.Column<string>(nullable: true),
                    ProcessingReportRecipients = table.Column<string>(nullable: true),
                    CdsFallbackEmail = table.Column<string>(nullable: true),
                    FisFallbackEmail = table.Column<string>(nullable: true),
                    ReportRootFolderName = table.Column<string>(nullable: true),
                    ProcessingFilesRootFolderName = table.Column<string>(nullable: true),
                    IngestionFilesRootFolderName = table.Column<string>(nullable: true),
                    QueueDataKey = table.Column<string>(nullable: true),
                    RpaTeamEmail = table.Column<string>(nullable: true),
                    MaxConsecutiveSystemExceptions = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Configs_Handlers_HandlerId",
                        column: x => x.HandlerId,
                        principalTable: "Handlers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    BotId = table.Column<string>(nullable: true),
                    Process = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    UnitInfoId = table.Column<Guid>(nullable: true),
                    FailureException = table.Column<string>(nullable: true),
                    FailureSource = table.Column<int>(nullable: false),
                    StatusMessage = table.Column<string>(nullable: true),
                    HandlerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Units_Handlers_HandlerId",
                        column: x => x.HandlerId,
                        principalTable: "Handlers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TraceParents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Data = table.Column<string>(nullable: true),
                    UnitParentId = table.Column<Guid>(nullable: true),
                    HandlerParentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TraceParents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TraceParents_HandlerParents_HandlerParentId",
                        column: x => x.HandlerParentId,
                        principalTable: "HandlerParents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TraceParents_UnitParents_UnitParentId",
                        column: x => x.UnitParentId,
                        principalTable: "UnitParents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UnitInfoParents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Data = table.Column<string>(nullable: true),
                    UnitParentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitInfoParents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UnitInfoParents_UnitParents_UnitParentId",
                        column: x => x.UnitParentId,
                        principalTable: "UnitParents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Traces",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    HandlerId = table.Column<Guid>(nullable: true),
                    UnitId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Traces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Traces_Handlers_HandlerId",
                        column: x => x.HandlerId,
                        principalTable: "Handlers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Traces_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UnitInfos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AttemptNumber = table.Column<int>(nullable: false),
                    UnitId = table.Column<Guid>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    ClaimNumber = table.Column<double>(nullable: true),
                    ClaimModifier = table.Column<string>(nullable: true),
                    Adjuster = table.Column<string>(nullable: true),
                    AdjusterEmail = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: true),
                    InvoiceId = table.Column<string>(nullable: true),
                    DaysBilled = table.Column<int>(nullable: true),
                    DaysAuthorized = table.Column<int>(nullable: true),
                    CdsClaimStatus = table.Column<int>(nullable: true),
                    FisClaimStatus = table.Column<int>(nullable: true),
                    LossCode = table.Column<string>(nullable: true),
                    Policy = table.Column<string>(nullable: true),
                    Tasks = table.Column<string>(nullable: true),
                    Reserves = table.Column<string>(nullable: true),
                    PaymentReserve = table.Column<string>(nullable: true),
                    RentalTask = table.Column<string>(nullable: true),
                    ReserveInfo = table.Column<string>(nullable: true),
                    RentalStartDate = table.Column<DateTime>(nullable: true),
                    RentalEndDate = table.Column<DateTime>(nullable: true),
                    DuplicatePayment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UnitInfos_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConfigParents_HandlerParentId",
                table: "ConfigParents",
                column: "HandlerParentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Configs_HandlerId",
                table: "Configs",
                column: "HandlerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TraceParents_HandlerParentId",
                table: "TraceParents",
                column: "HandlerParentId");

            migrationBuilder.CreateIndex(
                name: "IX_TraceParents_UnitParentId",
                table: "TraceParents",
                column: "UnitParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Traces_HandlerId",
                table: "Traces",
                column: "HandlerId");

            migrationBuilder.CreateIndex(
                name: "IX_Traces_UnitId",
                table: "Traces",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_UnitInfoParents_UnitParentId",
                table: "UnitInfoParents",
                column: "UnitParentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UnitInfos_UnitId",
                table: "UnitInfos",
                column: "UnitId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UnitParents_HandlerParentId",
                table: "UnitParents",
                column: "HandlerParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Units_HandlerId",
                table: "Units",
                column: "HandlerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConfigParents");

            migrationBuilder.DropTable(
                name: "Configs");

            migrationBuilder.DropTable(
                name: "TraceParents");

            migrationBuilder.DropTable(
                name: "Traces");

            migrationBuilder.DropTable(
                name: "UnitInfoParents");

            migrationBuilder.DropTable(
                name: "UnitInfos");

            migrationBuilder.DropTable(
                name: "UnitParents");

            migrationBuilder.DropTable(
                name: "Units");

            migrationBuilder.DropTable(
                name: "HandlerParents");

            migrationBuilder.DropTable(
                name: "Handlers");
        }
    }
}
