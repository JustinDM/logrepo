﻿// <auto-generated />
using System;
using Economical.LogRepo.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace LogRepo.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.31");

            modelBuilder.Entity("Economical.EcoObjects.General.Models.Config", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Discriminator")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<Guid>("HandlerId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("HandlerId")
                        .IsUnique();

                    b.ToTable("Configs");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Config");
                });

            modelBuilder.Entity("Economical.EcoObjects.General.Models.Handler", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("BotId")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("ConfigId")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("EndDate")
                        .HasColumnType("TEXT");

                    b.Property<int>("Process")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("TEXT");

                    b.Property<int>("Status")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("Handlers");
                });

            modelBuilder.Entity("Economical.EcoObjects.General.Models.Trace", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("Date")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("HandlerId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Message")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("UnitId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("HandlerId");

                    b.HasIndex("UnitId");

                    b.ToTable("Traces");
                });

            modelBuilder.Entity("Economical.EcoObjects.General.Models.Unit", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("BotId")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("EndDate")
                        .HasColumnType("TEXT");

                    b.Property<string>("FailureException")
                        .HasColumnType("TEXT");

                    b.Property<int>("FailureSource")
                        .HasColumnType("INTEGER");

                    b.Property<Guid>("HandlerId")
                        .HasColumnType("TEXT");

                    b.Property<int>("Process")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("TEXT");

                    b.Property<int>("Status")
                        .HasColumnType("INTEGER");

                    b.Property<string>("StatusMessage")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("UnitInfoId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("HandlerId");

                    b.ToTable("Units");
                });

            modelBuilder.Entity("Economical.EcoObjects.General.Models.UnitInfo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<int>("AttemptNumber")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Discriminator")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<Guid>("UnitId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("UnitId")
                        .IsUnique();

                    b.ToTable("UnitInfos");

                    b.HasDiscriminator<string>("Discriminator").HasValue("UnitInfo");
                });

            modelBuilder.Entity("Economical.EcoObjects.LogRepo.ConfigParent", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Data")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("HandlerParentId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("HandlerParentId")
                        .IsUnique();

                    b.ToTable("ConfigParents");
                });

            modelBuilder.Entity("Economical.EcoObjects.LogRepo.HandlerParent", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Data")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("HandlerParents");
                });

            modelBuilder.Entity("Economical.EcoObjects.LogRepo.TraceParent", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Data")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("HandlerParentId")
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("UnitParentId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("HandlerParentId");

                    b.HasIndex("UnitParentId");

                    b.ToTable("TraceParents");
                });

            modelBuilder.Entity("Economical.EcoObjects.LogRepo.UnitInfoParent", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Data")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("UnitParentId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("UnitParentId")
                        .IsUnique();

                    b.ToTable("UnitInfoParents");
                });

            modelBuilder.Entity("Economical.EcoObjects.LogRepo.UnitParent", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Data")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("HandlerParentId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("HandlerParentId");

                    b.ToTable("UnitParents");
                });

            modelBuilder.Entity("Economical.EcoObjects.EnterpriseRentalPayments.ErpConfig", b =>
                {
                    b.HasBaseType("Economical.EcoObjects.General.Models.Config");

                    b.Property<string>("CdsCredentialAssetName")
                        .HasColumnType("TEXT");

                    b.Property<string>("CdsFallbackEmail")
                        .HasColumnType("TEXT");

                    b.Property<string>("CdsQueueName")
                        .HasColumnType("TEXT");

                    b.Property<string>("EmailBodyFileName")
                        .HasColumnType("TEXT");

                    b.Property<string>("EmailConfigAssetName")
                        .HasColumnType("TEXT");

                    b.Property<string>("EmailCredentialsAssetName")
                        .HasColumnType("TEXT");

                    b.Property<string>("FisFallbackEmail")
                        .HasColumnType("TEXT");

                    b.Property<string>("FisQueueName")
                        .HasColumnType("TEXT");

                    b.Property<string>("GcpKeyPathAssetName")
                        .HasColumnType("TEXT");

                    b.Property<string>("IngestionFilesRootFolderName")
                        .HasColumnType("TEXT");

                    b.Property<int>("MaxConsecutiveSystemExceptions")
                        .HasColumnType("INTEGER");

                    b.Property<string>("NoteFileName")
                        .HasColumnType("TEXT");

                    b.Property<string>("NotificationSubject")
                        .HasColumnType("TEXT");

                    b.Property<string>("ProcessingFilesRootFolderName")
                        .HasColumnType("TEXT");

                    b.Property<string>("ProcessingReportRecipients")
                        .HasColumnType("TEXT");

                    b.Property<string>("QueueDataKey")
                        .HasColumnType("TEXT");

                    b.Property<string>("ReferenceText")
                        .HasColumnType("TEXT");

                    b.Property<string>("RepoCallerEndpointAssetName")
                        .HasColumnType("TEXT");

                    b.Property<string>("ReportEmailSubject")
                        .HasColumnType("TEXT");

                    b.Property<string>("ReportName")
                        .HasColumnType("TEXT");

                    b.Property<string>("ReportRootFolderName")
                        .HasColumnType("TEXT");

                    b.Property<string>("ReserveTableFileName")
                        .HasColumnType("TEXT");

                    b.Property<string>("ReserveTablePathAssetName")
                        .HasColumnType("TEXT");

                    b.Property<string>("ResourceFolderPath")
                        .HasColumnType("TEXT");

                    b.Property<string>("RpaTeamEmail")
                        .HasColumnType("TEXT");

                    b.Property<string>("TempFolderPath")
                        .HasColumnType("TEXT");

                    b.Property<string>("Timeout")
                        .HasColumnType("TEXT");

                    b.Property<string>("VendorCode")
                        .HasColumnType("TEXT");

                    b.Property<string>("VendorName")
                        .HasColumnType("TEXT");

                    b.HasDiscriminator().HasValue("ErpConfig");
                });

            modelBuilder.Entity("Economical.EcoObjects.EnterpriseRentalPayments.ErpUnitInfo", b =>
                {
                    b.HasBaseType("Economical.EcoObjects.General.Models.UnitInfo");

                    b.Property<string>("Adjuster")
                        .HasColumnType("TEXT");

                    b.Property<string>("AdjusterEmail")
                        .HasColumnType("TEXT");

                    b.Property<double>("Amount")
                        .HasColumnType("REAL");

                    b.Property<int>("CdsClaimStatus")
                        .HasColumnType("INTEGER");

                    b.Property<string>("ClaimModifier")
                        .HasColumnType("TEXT");

                    b.Property<double>("ClaimNumber")
                        .HasColumnType("REAL");

                    b.Property<int>("DaysAuthorized")
                        .HasColumnType("INTEGER");

                    b.Property<int>("DaysBilled")
                        .HasColumnType("INTEGER");

                    b.Property<string>("DuplicatePayment")
                        .HasColumnType("TEXT");

                    b.Property<int>("FisClaimStatus")
                        .HasColumnType("INTEGER");

                    b.Property<string>("InvoiceId")
                        .HasColumnType("TEXT");

                    b.Property<string>("LossCode")
                        .HasColumnType("TEXT");

                    b.Property<string>("PaymentReserve")
                        .HasColumnType("TEXT");

                    b.Property<string>("Policy")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("RentalEndDate")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("RentalStartDate")
                        .HasColumnType("TEXT");

                    b.Property<string>("RentalTask")
                        .HasColumnType("TEXT");

                    b.Property<string>("ReserveInfo")
                        .HasColumnType("TEXT");

                    b.Property<string>("Reserves")
                        .HasColumnType("TEXT");

                    b.Property<string>("Tasks")
                        .HasColumnType("TEXT");

                    b.HasDiscriminator().HasValue("ErpUnitInfo");
                });

            modelBuilder.Entity("Economical.EcoObjects.General.Models.Config", b =>
                {
                    b.HasOne("Economical.EcoObjects.General.Models.Handler", "Handler")
                        .WithOne("Config")
                        .HasForeignKey("Economical.EcoObjects.General.Models.Config", "HandlerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Economical.EcoObjects.General.Models.Trace", b =>
                {
                    b.HasOne("Economical.EcoObjects.General.Models.Handler", "Handler")
                        .WithMany("Traces")
                        .HasForeignKey("HandlerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Economical.EcoObjects.General.Models.Unit", "Unit")
                        .WithMany("Traces")
                        .HasForeignKey("UnitId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Economical.EcoObjects.General.Models.Unit", b =>
                {
                    b.HasOne("Economical.EcoObjects.General.Models.Handler", "Handler")
                        .WithMany("Units")
                        .HasForeignKey("HandlerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Economical.EcoObjects.General.Models.UnitInfo", b =>
                {
                    b.HasOne("Economical.EcoObjects.General.Models.Unit", "Unit")
                        .WithOne("UnitInfo")
                        .HasForeignKey("Economical.EcoObjects.General.Models.UnitInfo", "UnitId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Economical.EcoObjects.LogRepo.ConfigParent", b =>
                {
                    b.HasOne("Economical.EcoObjects.LogRepo.HandlerParent", "HandlerParent")
                        .WithOne("ConfigParent")
                        .HasForeignKey("Economical.EcoObjects.LogRepo.ConfigParent", "HandlerParentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Economical.EcoObjects.LogRepo.TraceParent", b =>
                {
                    b.HasOne("Economical.EcoObjects.LogRepo.HandlerParent", "HandlerParent")
                        .WithMany("TraceParents")
                        .HasForeignKey("HandlerParentId");

                    b.HasOne("Economical.EcoObjects.LogRepo.UnitParent", "UnitParent")
                        .WithMany("TraceParents")
                        .HasForeignKey("UnitParentId");
                });

            modelBuilder.Entity("Economical.EcoObjects.LogRepo.UnitInfoParent", b =>
                {
                    b.HasOne("Economical.EcoObjects.LogRepo.UnitParent", "UnitParent")
                        .WithOne("UnitInfoParent")
                        .HasForeignKey("Economical.EcoObjects.LogRepo.UnitInfoParent", "UnitParentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Economical.EcoObjects.LogRepo.UnitParent", b =>
                {
                    b.HasOne("Economical.EcoObjects.LogRepo.HandlerParent", "HandlerParent")
                        .WithMany("UnitParents")
                        .HasForeignKey("HandlerParentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
